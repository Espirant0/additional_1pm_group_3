#include <iostream>


void SumOfDigits(int N, int& sum) {
    if (N == 0)
        return;
    sum += N % 10;
    N /= 10;
    SumOfDigits(N, sum);
}


int main() {
    int N;
    int sum = 0;
    std::cin >> N;
    SumOfDigits(N, sum);
    std::cout << "sum = " << sum << std::endl;
    return 0;
}