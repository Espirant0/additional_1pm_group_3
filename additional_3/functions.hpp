#pragma once
#include <iostream>

namespace fal
{
	struct D_List
	{
		D_List* next; 
		D_List* prev; 
		int data;
	};

	class DLCL
	{
		D_List* head;
		D_List* tail;
		D_List* m_sent;
		int count;

	public:
		DLCL();

		~DLCL();

		

		void ADDINHEAD(int data); 
		void ADDINTAIL(int data); 
		void INSERT(int pos, int data);  
		void REVERSEPRINT();
		void DELETE(int pos); 

		void PRINT();

		void CLEAR();
	};
};