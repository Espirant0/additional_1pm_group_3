#include "bmp.hpp"
#include <fstream>
#include <iostream>


namespace saw {
    BMP::BMP() {}
    BMP::~BMP() {
        for (int i = 0; i < m_info.Height; i++)
            delete[] m_pixels[i];
        delete[] m_pixels;

        for (int i = 0; i < m_info_new.Height; i++)
            delete[] m_pixels_new[i];
        delete[] m_pixels_new;
    }

    void BMP::read() {
        std::ifstream in("in.bmp", std::ios::binary);
        in.read(reinterpret_cast<char*>(&m_header), sizeof(BMPHEADER));

        in.read(reinterpret_cast<char*>(&m_info), sizeof(BMPINFO));

        m_pixels = new Pixel * [m_info.Height];
        for (int i = 0; i < m_info.Height; i++)
            m_pixels[i] = new Pixel[m_info.Width];

        for (int i = 0; i < m_info.Height; i++) {
            for (int j = 0; j < m_info.Width; j++)
                in.read(reinterpret_cast<char*>(&m_pixels[i][j]), sizeof(Pixel));

            if ((3 * m_info.Width) % 4 != 0)
                for (int j = 0; j < 4 - (3 * m_info.Width) % 4; j++)
                {
                    char c;
                    in.read(&c, 1);
                }
        }


        BMP::m_pixels_new = new Pixel * [m_info.Height];
        for (int i = 0; i < m_info.Height; i++)
            m_pixels_new[i] = new Pixel[m_info.Width];
        for (int i = 0; i < m_info.Height; i++)
        {
            for (int j = 0; j < m_info.Width; j++)
            {
                m_pixels_new[i][j] = m_pixels[i][j];
            }
        }
    }

    void BMP::print() {
        std::ofstream out("out.bmp", std::ios::binary);
        int width = m_info.Width;
        int height = m_info.Height;

        m_header_new.Type = 0x4D42;
        m_header_new.Size = 14 + 40 + (3 * width * height);
        if (width % 4 != 0)
            m_header_new.Size += (4 - (3 * width) % 4) * height;
        m_header_new.OffBits = 54;
        m_header_new.Reserved1 = 0;
        m_header_new.Reserved2 = 0;

        out.write(reinterpret_cast<char*>(&m_header_new), sizeof(BMPHEADER));

        m_info_new.BitCount = 24;
        m_info_new.ClrImportant = 0;
        m_info_new.ClrUsed = 0;
        m_info_new.Compression = 0;
        m_info_new.Height = height;
        m_info_new.Planes = 1;
        m_info_new.Size = 40;
        m_info_new.SizeImage = m_header_new.Size - 54;
        m_info_new.Width = width;
        m_info_new.XPelsPerMeter = 0;
        m_info_new.YPelsPerMeter = 0;

        out.write(reinterpret_cast<char*>(&m_info_new), sizeof(BMPINFO));


        for (int i = 0; i < m_info_new.Height; i++)
        {
            for (int j = 0; j < m_info_new.Width; j++)
                out.write(reinterpret_cast<char*>(&m_pixels_new[i][j]), sizeof(Pixel));

            if ((3 * m_info_new.Width) % 4 != 0)
                for (int j = 0; j < 4 - (3 * m_info_new.Width) % 4; j++)
                {
                    char c = 0;
                    out.write(&c, 1);
                }
        }

    }

    void BMP::filter() {
        for (int i = 0; i < m_info.Height; i++)
            for (int j = 0; j < m_info.Width; j++)
            {
                m_pixels_new[i][j].b = m_pixels_new[i][j].b - 50;
                m_pixels_new[i][j].r = m_pixels_new[i][j].r + 50;
                m_pixels_new[i][j].g = m_pixels_new[i][j].g + 25;
            }
    }
    void BMP::code(char s[4], int d) {
        int i = 0;
        for (int n = 0; n < d; n++) {
            if ((s[0] == 'p') && (s[1] == 'a') && (s[2] == 's') && (s[3] == 's')) {
                m_pixels_new[20][i + 1].b = 241;
                m_pixels_new[20][i + 2].r = 241;
                m_pixels_new[20][i + 3].g = 241;
                m_pixels_new[21][i + 1].b = 241;
                m_pixels_new[21][i + 2].r = 241;
                m_pixels_new[21][i + 3].g = 241;
                m_pixels_new[22][i + 1].b = 241;
                m_pixels_new[22][i + 2].r = 241;
                m_pixels_new[22][i + 3].g = 241;
                m_pixels_new[23][i + 1].b = 241;
                m_pixels_new[23][i + 2].r = 241;
                m_pixels_new[23][i + 3].g = 241;
                i += 1;
            }
            i += 1;

        }
    }


    void BMP::anticode() {

        for (int i = 0; i < m_info.Height; i++)
            for (int j = 0; j < m_info.Width; j++)
            {
                m_pixels_new[i][j].g *= 100;
            }
    }
};