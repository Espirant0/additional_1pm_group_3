#pragma once
#include <fstream>
#include <iostream>
#define N 4


namespace saw {
#pragma pack(1) 
    struct BMPHEADER
    {
        unsigned short    Type;
        unsigned int      Size;
        unsigned short    Reserved1;
        unsigned short    Reserved2;
        unsigned int      OffBits;
    };
#pragma pack()

#pragma pack(1)
    struct BMPINFO
    {
        unsigned int    Size;
        int             Width;
        int             Height;
        unsigned short  Planes;
        unsigned short  BitCount;
        unsigned int    Compression;
        unsigned int    SizeImage;
        int             XPelsPerMeter;
        int             YPelsPerMeter;
        unsigned int    ClrUsed;
        unsigned int    ClrImportant;
    };
#pragma pack()

#pragma pack(1)
    struct Pixel
    {
        unsigned char b;
        unsigned char g;
        unsigned char r;
    };
#pragma pack()
    class BMP {
    public:
        BMP();
        ~BMP();
        void read();
        void print();
        void filter();
        void code(char s[4], int d);
        void anticode();
    private:
        BMPHEADER m_header;
        BMPINFO m_info;
        Pixel** m_pixels;
        Pixel** m_pixels_new;
        BMPHEADER m_header_new;
        BMPINFO m_info_new;
    };
};

