#include <iostream>

class Foo {
public:
    Foo(int j) {
        n = j; //*****
        i = new int[j];

        for (int k = 0; k < n; k++) { //*****
            i[k] = k * 2 + 1;
        }
    }

    void print() { //*****
        for (int k = 0; k < n; k++) {
            std::cout << i[k] << std::endl;
        }
    }

    Foo(const Foo& object) : i(object.i) {   //*****
    }

    Foo& operator=(const Foo& object) {  //*****
        return *this;
    }

    virtual ~Foo() { //*****
        delete[] i;
    }
protected: //*****
    int* i;
    int n;
};

class Bar : public Foo { //*****
public:
    Bar(int j) : Foo(j) { //*****
        i = new char[j];
    }

    virtual ~Bar() { //*****
        delete[] i; //*****
    }
protected: //*****
    char* i;
};

int main() { //*****
    Foo* f = new Foo(5);
    Foo* b = new Bar(6);

    f->print();    //***** 
    std::cout << std::endl;
    b->print();
    std::cout << std::endl;

    *f = *b;

    f->print();
    std::cout << std::endl;
    b->print();
    std::cout << std::endl;

    delete f;
    delete b;
    return 0;
}


